import java.util.Random;

public class HeavyAttack extends Attack {
    @Override
    public String getName() {
        return "Do 10 damage with 50% chance";
    }

    @Override
    public void doAttack(Player first, Player second) {
        Random random = new Random();
        if (random.nextInt(10) >= 5) {
            second.damage(10);
            System.out.println(first.getName() + " made 10 damage to " + second.getName());
        }
        else {
            System.out.println(first.getName() + " missed");
        }
    }
}