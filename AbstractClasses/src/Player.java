import java.util.Random;

public class Player {
    private String name;
    private int hp;

    private Attack[] attacks;


    public Player(String name, int hp, Attack[] attacks) {
        this.name = name;
        this.hp = hp;
        this.attacks = attacks;
    }


    public String getName() {
        return this.name;
    }

    public int getHp() {
        return this.hp;
    }

    public void damage(int value) {
        this.hp -= value;
    }

    public boolean isAlive() {
        return this.hp > 0;
    }

    public Attack[] getAttacks() {
        return this.attacks;
    }


    public void doAttack(int id, Player other) {
        attacks[id].doAttack(this, other);
    }
}