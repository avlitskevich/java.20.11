import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter hp value:");

        Attack[] availableAttacks = new Attack[3];
        availableAttacks[0] = new SimpleAttack();
        availableAttacks[1] = new HeavyAttack();
        availableAttacks[2] = new InstantAttack();

        Game game = new Game(sc.nextInt(), availableAttacks);

        while (true) {
            game.makeNewPlayers();
            game.makeGame();

            System.out.println("Do you want to play another time?");
            System.out.println("Enter 0 for yes, any other number for exit");

            int answer = sc.nextInt();
            if (answer != 0)
                break;
        }
    }
}
