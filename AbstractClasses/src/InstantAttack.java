import java.util.Random;

public class InstantAttack extends Attack {
    @Override
    public String getName() {
        return "Kill other player with 10% chance";
    }

    @Override
    public void doAttack(Player first, Player second) {
        Random random = new Random();
        if (random.nextInt() >= 9) {
            second.damage(second.getHp());
            System.out.println(first.getName() + " killed " + second.getName());
        }
        else {
            System.out.println(first.getName() + " missed");
        }
    }
}