public abstract class Attack {
    public abstract String getName();

    public abstract void doAttack(Player first, Player second);
}