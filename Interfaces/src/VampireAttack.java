public class VampireAttack implements IAttack, IHeal {
    @Override
    public void doAttack(Player first, Player second) {
        second.damage(5);
        System.out.println(first.getName() + " made 5 damage to " + second.getName());
    }

    @Override
    public String getName() {
        return "Make a 5 vampire damage and heal self for 5 hp";
    }

    @Override
    public void heal(Player player) {
        player.heal(5);
        System.out.println(player.getName() + " healed with 5 hp");
    }
}