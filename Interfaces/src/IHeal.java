public interface IHeal {
    public void heal(Player player);

    public String getName();
}