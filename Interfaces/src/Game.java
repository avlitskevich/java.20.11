import java.util.ArrayList;
import java.util.Scanner;

public class Game {
    private int defaultHpValue;
    private int counter;

    private Player currentPlayer;
    private Player anotherPlayer;

    private ArrayList<IAttack> availableAttacks;
    private ArrayList<IHeal> availableHeals;


    public Game(int defaultHpValue, ArrayList<IAttack> availableAttacks, ArrayList<IHeal> availableHeals) {
        this.defaultHpValue = defaultHpValue;
        this.availableAttacks = availableAttacks;
        this.availableHeals = availableHeals;
    }


    public void makeNewPlayers() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter first player name:");
        currentPlayer = new Player(sc.nextLine(), defaultHpValue);
        System.out.println("Enter first player name:");
        anotherPlayer = new Player(sc.nextLine(), defaultHpValue);
    }

    public void makeGame() {
        counter = 0;

        while (checkAlive())
            makeTurn();

        printWinner();
    }


    private void makeTurn() {
        Scanner sc = new Scanner(System.in);

        printTurnInfo();
        printAttackInfo();

        int attack = sc.nextInt();
        attack = handleAttackInput(attack);
        proceedPlayerAttack(attack);

        printHealsInfo();

        int heal = sc.nextInt();
        heal = handleHealInput(heal);
        proceedPlayerHeal(heal);

        switchPlayers();

        counter++;
        if (counter == 4) {
            availableAttacks.add(new InstantAttack());
        }
    }

    private boolean checkAlive() {
        return currentPlayer.isAlive() && anotherPlayer.isAlive();
    }

    private void printLine() {
        System.out.println("");
        System.out.println("-----------------------------");
        System.out.println("");
    }

    private void printTurnInfo() {
        printLine();
        printPlayersInfo();

        System.out.println("Now it's " + currentPlayer.getName() + " turn");
    }

    private void printPlayersInfo() {
        System.out.println(currentPlayer.getName() + ": " + currentPlayer.getHp());
        System.out.println(anotherPlayer.getName() + ": " + anotherPlayer.getHp());
    }

    private void printAttackInfo() {
        System.out.println("");

        System.out.println("Choose your attack:");

        for (int i = 0; i < availableAttacks.size(); i++) {
            System.out.println((i + 1) + ": " + availableAttacks.get(i).getName());
        }
    }

    private void printHealsInfo() {
        System.out.println("");

        System.out.println("Choose your heal:");

        for (int i = 0; i < availableHeals.size(); i++) {
            System.out.println((i + 1) + ": " + availableHeals.get(i).getName());
        }
    }

    private int handleAttackInput(int attack) {
        attack--;
        if (attack < 0)
            attack = 0;
        if (attack >= availableAttacks.size())
            attack = availableAttacks.size() - 1;
        return attack;
    }

    private int handleHealInput(int heal) {
        heal--;
        if (heal < 0)
            heal = 0;
        if (heal >= availableHeals.size())
            heal = availableHeals.size() - 1;
        return heal;
    }

    private void proceedPlayerAttack(int attack) {
        availableAttacks.get(attack).doAttack(currentPlayer, anotherPlayer);
    }

    private void proceedPlayerHeal(int heal) {
        availableHeals.get(heal).heal(currentPlayer);
    }

    private void switchPlayers() {
        Player temp = currentPlayer;
        currentPlayer = anotherPlayer;
        anotherPlayer = temp;
    }

    private void printWinner() {
        printLine();
        printPlayersInfo();

        Player winner = currentPlayer.isAlive() ? currentPlayer : anotherPlayer;
        System.out.println("Winner: " + winner.getName());
    }
}