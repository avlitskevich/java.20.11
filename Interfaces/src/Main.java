import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter hp value:");

        ArrayList<IAttack> availableAttacks = new ArrayList<>();
        availableAttacks.add(new SimpleAttack());
        availableAttacks.add(new HeavyAttack());
        availableAttacks.add(new VampireAttack());

        ArrayList<IHeal> availableHeals = new ArrayList<>();
        availableHeals.add(new Heal());
        availableHeals.add(new VampireAttack());

        Game game = new Game(sc.nextInt(), availableAttacks, availableHeals);

        while (true) {
            game.makeNewPlayers();
            game.makeGame();

            System.out.println("Do you want to play another time?");
            System.out.println("Enter 0 for yes, any other number for exit");

            int answer = sc.nextInt();
            if (answer != 0)
                break;
        }
    }
}
