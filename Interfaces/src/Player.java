import java.util.Random;

public class Player {
    private String name;
    private int hp;


    public Player(String name, int hp) {
        this.name = name;
        this.hp = hp;
    }


    public String getName() {
        return this.name;
    }

    public int getHp() {
        return this.hp;
    }

    public void damage(int value) {
        this.hp -= value;
    }

    public void heal(int value) {
        this.hp += value;
    }

    public boolean isAlive() {
        return this.hp > 0;
    }
}