import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter hp value:");

        ArrayList<Action> availableActions = new ArrayList<>();
        availableActions.add(new SimpleAttack());
        availableActions.add(new HeavyAttack());
        availableActions.add(new InstantAttack());
        availableActions.add(new Heal());
        availableActions.add(new VampireAttack());

        Game game = new Game(sc.nextInt(), availableActions);

        while (true) {
            game.makeNewPlayers();
            game.makeGame();

            System.out.println("Do you want to play another time?");
            System.out.println("Enter 0 for yes, any other number for exit");

            int answer = sc.nextInt();
            if (answer != 0)
                break;
        }
    }
}
