import java.util.ArrayList;
import java.util.Scanner;

public class Game {
    private int defaultHpValue;
    private int counter;

    private Player currentPlayer;
    private Player anotherPlayer;

    private ArrayList<Action> availableActions;


    public Game(int defaultHpValue, ArrayList<Action> availableActions) {
        this.defaultHpValue = defaultHpValue;
        this.availableActions = availableActions;
    }


    public void makeNewPlayers() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter first player name:");
        currentPlayer = new Player(sc.nextLine(), defaultHpValue);
        System.out.println("Enter first player name:");
        anotherPlayer = new Player(sc.nextLine(), defaultHpValue);
    }

    public void makeGame() {
        counter = 0;

        while (checkAlive())
            makeTurn();

        printWinner();
    }


    private void makeTurn() {
        Scanner sc = new Scanner(System.in);

        printTurnInfo();
        printActionsInfo();

        int action = sc.nextInt();
        action = handleActionInput(action);
        proceedPlayerAction(action);

        switchPlayers();

        counter++;
        if (counter == 4) {
            availableActions.add(new InstantAttack());
        }
    }

    private boolean checkAlive() {
        return currentPlayer.isAlive() && anotherPlayer.isAlive();
    }

    private void printLine() {
        System.out.println("");
        System.out.println("-----------------------------");
        System.out.println("");
    }

    private void printTurnInfo() {
        printLine();
        printPlayersInfo();

        System.out.println("Now it's " + currentPlayer.getName() + " turn");
    }

    private void printPlayersInfo() {
        System.out.println(currentPlayer.getName() + ": " + currentPlayer.getHp());
        System.out.println(anotherPlayer.getName() + ": " + anotherPlayer.getHp());
    }

    private void printActionsInfo() {
        System.out.println("");

        System.out.println("Choose your action:");

        for (int i = 0; i < availableActions.size(); i++) {
            System.out.println((i + 1) + ": " + availableActions.get(i).getName());
        }
    }

    private int handleActionInput(int action) {
        action--;
        if (action < 0)
            action = 0;
        if (action >= availableActions.size())
            action = availableActions.size() - 1;
        return action;
    }

    private void proceedPlayerAction(int action) {
        Action chosenAction = availableActions.get(action);

        if (chosenAction instanceof IAttack) {
            IAttack attack = (IAttack) chosenAction;
            attack.doAttack(currentPlayer, anotherPlayer);
        }

        if (chosenAction instanceof IHeal) {
            IHeal heal = (IHeal) chosenAction;
            heal.heal(currentPlayer);
        }
    }

    private void switchPlayers() {
        Player temp = currentPlayer;
        currentPlayer = anotherPlayer;
        anotherPlayer = temp;
    }

    private void printWinner() {
        printLine();
        printPlayersInfo();

        Player winner = currentPlayer.isAlive() ? currentPlayer : anotherPlayer;
        System.out.println("Winner: " + winner.getName());
    }
}