public class SimpleAttack extends Action implements IAttack {
    @Override
    public String getName() {
        return "Do 5 damage with 100% chance";
    }

    @Override
    public void doAttack(Player first, Player second) {
        second.damage(5);
        System.out.println(first.getName() + " made 5 damage to " + second.getName());
    }
}
