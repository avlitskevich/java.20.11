public class Heal extends Action implements IHeal {
    @Override
    public void heal(Player player) {
        player.heal(5);
    }

    @Override
    public String getName() {
        return "Heal player with 5 hp";
    }
}